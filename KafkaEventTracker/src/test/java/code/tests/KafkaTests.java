package code.tests;

import code.ConfigMe;
import code.utils.AccessJsonFile;
import code.utils.AssertionUtil;
import code.utils.MyException;
import code.utils.MyStringUtil;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Vijay on 6/6/2016.
 */
@Listeners({org.uncommons.reportng.HTMLReporter.class, org.uncommons.reportng.JUnitXMLReporter.class})
public class KafkaTests {

    private static ArrayList<String> actualResponsesFromFileToArrayList;
    private static AccessJsonFile dataFeedsFromJsonFile;
    private static ArrayList<String> checksForMylogsFileGenerator = new ArrayList<String>();
    private static ArrayList<String> resultListOfMyEvents;
    private static File mylogsFile;

    @BeforeSuite
    public static void loadkafkaResponses() throws IOException, ParseException {
        File logFile = new File(ConfigMe.logFilePath);
        mylogsFile = new File(ConfigMe.mylogsFile);
        String allResponseInString = FileUtils.readFileToString(logFile);
        actualResponsesFromFileToArrayList = new ArrayList<String>(Arrays.asList(allResponseInString.split(ConfigMe.lineSeparator)));
        dataFeedsFromJsonFile = new AccessJsonFile(ConfigMe.kafkaTestFeed);
    }

    @BeforeClass
    public static void getOnlyMyEventsBasedOnUIDAndTrait65() throws IOException, ParseException {
        checksForMylogsFileGenerator.add(ConfigMe.globalFilterUID);
        checksForMylogsFileGenerator.add(ConfigMe.globalFilterDevice);
        resultListOfMyEvents = MyStringUtil.getMatchedResponses(actualResponsesFromFileToArrayList, checksForMylogsFileGenerator);
        System.out.println("All Your Events Count in Logfile: " + resultListOfMyEvents.size());
        FileUtils.writeStringToFile(mylogsFile, MyStringUtil.getNewArrayListWithAddedString(resultListOfMyEvents, ConfigMe.lineSeparator).toString());
        System.out.println("All Your Events logged at : " + mylogsFile.getAbsolutePath());
    }

    @DataProvider(name = "feedKafkaTest")
    public Object[][] getEventsList() {
        Set<Object> keys = dataFeedsFromJsonFile.jsonObject.keySet();
        String[] eventsInArray = keys.toArray(new String[keys.size()]);
        String[][] listOfEvents = new String[keys.size()][1];
        for (int i = 0; i < keys.size(); i++) {
            for (int y = 0; y < 1; y++) {
                listOfEvents[i][y] = eventsInArray[i];
            }
        }
        return listOfEvents;
    }

    @Test(dataProvider = "feedKafkaTest")
    public void kafkaTests001(String eventName) throws MyException {
        System.out.println("Running Tests For Event :" + eventName);
        ArrayList<Integer> countsFromJson = dataFeedsFromJsonFile.getCountArrayForChecks(eventName);
        ArrayList<String> operatorsFromJson = dataFeedsFromJsonFile.getOperatorArrayForChecks(eventName);
        JSONObject checksFromJon = dataFeedsFromJsonFile.getChecksAsMap(eventName);

        for (int i = 1; i <= checksFromJon.size(); i++) {
            String checkName = "check" + i;
            System.out.println("Checking :" + checkName);
            //Json Filters
            ArrayList<String> listOfFiltersFromJson = new ArrayList<String>();
            listOfFiltersFromJson.add("\"event\":\"" + eventName + "\"");
            listOfFiltersFromJson.addAll(dataFeedsFromJsonFile.getInnerJsonArray(checksFromJon, checkName));
            //Global ConfigMe Filters
            ArrayList<String> globalFiltersInEachCheck = new ArrayList<String>(Arrays.asList(ConfigMe.globalFilters));

            //Final list of filters
            ArrayList<String> finalFilters = new ArrayList<String>();
            finalFilters.addAll(listOfFiltersFromJson);
            finalFilters.addAll(globalFiltersInEachCheck);

            int expectedCountFromJson = Integer.parseInt(String.valueOf(countsFromJson.get(i - 1)));
            String operatorFromJson = operatorsFromJson.get(i - 1);
            System.out.println("finalFilters" + finalFilters);
            int actualCountFromFile = MyStringUtil.getMatchedResponses(resultListOfMyEvents, finalFilters).size();
            if (!(AssertionUtil.myAssertion(expectedCountFromJson, operatorFromJson, actualCountFromFile))) {
                throw new MyException(AssertionUtil.getErrorMessage(expectedCountFromJson, operatorFromJson, actualCountFromFile));
            }
        }
    }
}
