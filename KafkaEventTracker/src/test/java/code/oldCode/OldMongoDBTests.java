//package code.tests;
//
//import code.ConfigMe;
//import code.mongodb.MongoCommunications;
//import code.utils.MyStringUtil;
//import com.mongodb.DBCollection;
//import com.mongodb.DBObject;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
///**
// * Created by Vijay on 6/6/2016.
// */
//public class OldMongoDBTests {
//
//    public static DBCollection resultCollection;
//    public static String documnetsOfGivenUID;
//    public static ArrayList<DBObject> matchedDBObjects;
//    public static MongoCommunications mongoCommunications;
//
//    @BeforeClass
//    public static void connectToDB() throws IOException {
//        mongoCommunications= new MongoCommunications(ConfigMe.serverAddress,ConfigMe.port,ConfigMe.dbName,ConfigMe.dbUserName,ConfigMe.dbPassword,ConfigMe.CollectionName);
//        resultCollection = mongoCommunications.collection;
//        documnetsOfGivenUID = mongoCommunications.getMatchedRecordsAsString(resultCollection,ConfigMe.UID_FiledName,ConfigMe.UID_Value);
//        matchedDBObjects = mongoCommunications.getMatchedRecords(resultCollection,ConfigMe.UID_FiledName,ConfigMe.UID_Value);
//    }
//
//    @Test
//    public void db_test001(){
//        Assert.assertTrue(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter11[0],ConfigMe.filter11[1]).size()>=ConfigMe.filter11Count);
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter12[0],ConfigMe.filter12[1]).size(),ConfigMe.filter12Count);
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter13[0],ConfigMe.filter13[1]).size(),ConfigMe.filter13Count);
//    }
//
////    @Test
////    public void db_test002(){
////        System.out.println("db_test002 "+MyStringUtil.getSubStringCount(documnetsOfGivenUID,ConfigMe.filter2));
////        Assert.assertTrue(MyStringUtil.getSubStringCount(documnetsOfGivenUID,ConfigMe.filter2)==ConfigMe.filter2Count2);
////    }
//
//    @Test
//    public void db_test003(){
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter31[0],ConfigMe.filter31[1]).size(),ConfigMe.filter7Count);
//        Assert.assertTrue(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter32[0],ConfigMe.filter32[1]).size()>=ConfigMe.filter7Count);
//    }
//
//    @Test
//    public void db_test004(){
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter4[0],ConfigMe.filter4[1]).size(),ConfigMe.filter4Count);
//    }
//
//    @Test
//    public void db_test005(){
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter5[0],ConfigMe.filter5[1]).size(),ConfigMe.filter7Count);
//    }
//
//    @Test
//    public void db_test006(){
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter6[0],ConfigMe.filter6[1]).size(),ConfigMe.filter6Count);
//    }
//
//    @Test
//    public void db_test007(){
//        Assert.assertEquals(mongoCommunications.getEachRecordWith(matchedDBObjects,ConfigMe.filter7[0],ConfigMe.filter7[1]).size(),ConfigMe.filter7Count);
//    }
//
//}
