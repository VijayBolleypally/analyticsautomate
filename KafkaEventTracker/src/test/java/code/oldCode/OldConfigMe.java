package code.oldCode;


/**
 * Created by Vijay on 6/3/2016.
 */
public class OldConfigMe {

    //Step 1
    //Kafka Events Tracker config
    public static String zookeeperConnect = "54.175.27.6:2181";
    public static String aGroupId = "QA-topic_consumer";
    public static String topicName = "Phenom_Track_YODLA0048_TOPIC";


    public static String UID_Value = "15529873d1479-100200-7eb8-15529873d15210";
    public static String Refnum = "YODLA0048";
    //get file
    public static String logFilePath = "C:\\Users\\imomadmin\\Desktop\\kafkaLogs.txt";
    public static String globalFilterUID = "\"uid\":\""+UID_Value+"\"";
    public static String globalFilterRefnum= "\"phenomRefnum\":\""+Refnum+"\"";

    //Run this in individual code.tests
    public static String[] check11 = {"\"createType\":\"pt_cookie\"","\"event\":\"pt_page_view\"","15529873d1479-100200-7eb8-15529873d15210"};
    public static String[] check12 = {"\"event\":\"page_search\"","\"trait2\":\"YODLA0048\"","\"trait6\":\"test\""};

    public static String[] check2 = {"\"event\":\"pt_page_view\"","\"pt_referrer\":\"https://yodle-uat.imomentous.co/search-results?keywords=test\""};

    public static String[] check3 = {"\"event\":\"header_menu_click\"","\"trait2\":\"YODLA0048\"","\"trait62\":\"Home\""};

    public static String[] check4 = {"\"event\":\"linkedin_login_click\"","\"trait2\":\"YODLA0048\""};

    public static String[] check5 = {"\"event\":\"no_search_results\"","\"trait2\":\"YODLA0048\"","\"trait6\":\"dummyyyyy\""};

    public static String[] check6 = {"\"event\":\"linkedin_recommended_job_click\"","\"trait2\":\"YODLA0048\"","\"trait5\":\"YODLA0048213\""};

    public static String[] check7 = {"\"event\":\"job_details_view\"","\"trait2\":\"YODLA0048\"","\"trait5\":\"YODLA0048213\""};

    public static String[] check8 = {"\"phenomRefnum\":\"YODLA0048\""};

    public static int check1Count = 1;
    public static int check2Count = 1;
    public static int check3Count = 1;
    public static int check4Count = 1;
    public static int check5Count = 1;
    public static int check6Count = 1;
    public static int check7Count = 1;


    //DB
    public static String serverAddress = "54.174.245.244";
    public static int port = 27017;

    public static String CollectionName = "YODLA0048";

    //non-flat
//    public static String dbName = "usertables_qa";
//    public static String dbUserName = "phegloqa";
//    public static String dbPassword = "goodDevelopers@1";

    //flat
    public static String dbName = "usertables_qa_flat";
    public static String dbUserName = "phegloqaflat";
    public static String dbPassword = "goodDevelopers@1";

    public static String UID_FiledName = "uid";


    public static String []filter11 = {"pagesViewed","home_page_view"};
    public static int filter11Count = 2;
    public static String []filter12 = {"search","\"keyword\" : \"test\""};
    public static int filter12Count = 1;
    public static String []filter13 = {"pagesViewed","search_result_page_view"};
    public static int filter13Count = 2;

//    public static String filter2 = "";

    public static String []filter31 = {"clicks","\"menuItem\" : \"Home\""};
    public static int filter31Count = 1;
    public static String []filter32 = {"clicks","header_menu_click"};
    public static int filter32Count = 1;


    public static String []filter4 = {"clicks","linkedin_login_click"};
    public static int filter4Count = 1;

    public static String []filter5 = {"noResultKeywords","\"keyword\" : \"dummyyyyy\""};
    public static int filter5Count = 1;

    public static String []filter6 = {"linkedINrecommendedJobs","YODLA0048213"};
    public static int filter6Count = 1;

    public static String []filter7 = {"jobsViewed","YODLA0048213"};
    public static int filter7Count = 1;

}
