//package code.tests;
//
//import code.ConfigMe;
//import code.utils.AccessJsonFile;
//import code.utils.AssertionUtil;
//import code.utils.MyException;
//import code.utils.MyStringUtil;
//import org.apache.commons.io.FileUtils;
//import org.json.simple.parser.ParseException;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Set;
//
///**
// * Created by Vijay on 6/6/2016.
// */
//public class OldKafkaTests1 {
//
//    private static ArrayList<String> actualResponsesFromFileToArrayList;
//    private static AccessJsonFile dataFeedsFromJsonFile;
//    private static String globalFilterUID = ConfigMe.globalFilterUID;
//    private static String globalFilterRefNo = ConfigMe.globalFilterRefnum;
//
//    @BeforeClass
//    public static void loadkafkaResponses() throws IOException, ParseException {
//        File logFile = new File(ConfigMe.logFilePath);
//        String allResponseInString = FileUtils.readFileToString(logFile);
//        actualResponsesFromFileToArrayList = new ArrayList<String>(Arrays.asList(allResponseInString.split(System.getProperty("line.separator"))));
//        dataFeedsFromJsonFile = new AccessJsonFile();
//    }
//
//    @DataProvider(name = "feedKafkaTest")
//    public Object[][] getEventsList() {
//        Set<Object> keys = dataFeedsFromJsonFile.jsonObject.keySet();
//        String[] eventsInArray = keys.toArray(new String[keys.size()]);
//        String[][] listOfEvents = new String[keys.size()][1];
//        for (int i = 0; i < keys.size(); i++) {
//            for (int y = 0; y < 1; y++) {
//                listOfEvents[i][y] = eventsInArray[i];
//            }
//        }
//        return listOfEvents;
//    }
//
//    @Test(dataProvider = "feedKafkaTest")
//    public void kafkaTests001(String eventName) throws MyException {
//        System.out.println("Running Tests For Event:" + eventName);
//        ArrayList<String> finalFilter = dataFeedsFromJsonFile.getTraitsForEvent(eventName);
//        finalFilter.add("\"event\":\"" + eventName + "\"");
//        finalFilter.add(globalFilterRefNo);
//        finalFilter.add(globalFilterUID);
//        int actualCount = MyStringUtil.getMatchedResponses(actualResponsesFromFileToArrayList, finalFilter).size();
//        String operator = dataFeedsFromJsonFile.getOperatorForEvent(eventName);
//        int expectedCount = dataFeedsFromJsonFile.getCountForEvent(eventName);
//        System.out.println(actualCount + ":" + expectedCount);
//        if (!(AssertionUtil.myAssertion(expectedCount, operator, actualCount))) {
//            throw new MyException(AssertionUtil.getErrorMessage(expectedCount, operator, actualCount));
//        }
//    }
//}
