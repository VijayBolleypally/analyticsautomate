//package code.tests;
//
//import code.utils.MyStringUtil;
//import org.apache.commons.io.FileUtils;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//
//import java.io.File;
//import java.io.IOException;
//
//import code.*;
//import org.junit.Test;
//
///**
// * Created by Vijay on 6/6/2016.
// */
//public class OldKafkaTests {
//
//    private static String responsesInArray[];
//
//    @BeforeClass
//    public static void loadkafkaResponses() throws IOException {
//        File logFile = new File(ConfigMe.logFilePath);
//        String allResponseInString = FileUtils.readFileToString(logFile);
//        responsesInArray = allResponseInString.split(System.getProperty("line.separator"));
//    }
//
//    @Test
//    public void kafka_test001() {
//        System.out.println("kafka_test001:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check11).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check11).size() == ConfigMe.check1Count);
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check12).size() == ConfigMe.check1Count);
//    }
//
//    @Test
//    public void kafka_test002() {
//        System.out.println("kafka_test002:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check2).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check2).size() == ConfigMe.check2Count);
//    }
//
//    @Test
//    public void kafka_test003() {
//        System.out.println("kafka_test003:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check3).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check3).size() == ConfigMe.check3Count);
//    }
//
//    @Test
//    public void kafka_test004() {
//        System.out.println("kafka_test004:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check4).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check4).size() == ConfigMe.check4Count);
//    }
//
//    @Test
//    public void kafka_test005() {
//        System.out.println("kafka_test005:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check5).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check5).size() == ConfigMe.check5Count);
//    }
//
//    @Test
//    public void kafka_test006() {
//        System.out.println("kafka_test006:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check5).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check6).size() == ConfigMe.check6Count);
//    }
//
//    @Test
//    public void kafka_test007() {
//        System.out.println("kafka_test007:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check5).size());
//        Assert.assertTrue(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check7).size() == ConfigMe.check7Count);
//    }
//
//    @Test
//    public void kafka_test008() {
//        System.out.println("kafka_test008:" + MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check8).size() + ":"+responsesInArray.length);
//        Assert.assertEquals(MyStringUtil.getMatchedResponses(responsesInArray, ConfigMe.check8).size() , responsesInArray.length);
//    }
//}
